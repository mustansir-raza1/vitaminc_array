function sortedItems(items) {
    const sortedItems = items.sort((a, b) => {
        return a.contains.split(', ').length + b.contains.split(', ').length;
    });
    return sortedItems;
}
module.exports = sortedItems;