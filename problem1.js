function available(data) {
    const dataAvailable = data.filter(items => items.available)
    return dataAvailable;
}
module.exports = available;