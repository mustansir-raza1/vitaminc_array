function getAllItemsGroupByVitamin(items){
    if(Array.isArray(items)){
       
        const allItemsGroupByVitamin = items.reduce((previousItems,currentItems) => {
            const itemName = currentItems.name;
            const vitamin = currentItems.contains.split(', ');
           
            vitamin.reduce((acc, vitamin) => {
                if (!acc[vitamin]) {
                  acc[vitamin] = [];
                }
                acc[vitamin].push(itemName);
                return acc;
              }, previousItems);
            return previousItems;
        },{})
        return allItemsGroupByVitamin;
    }
    else{
        return {};
    }
}

module.exports = getAllItemsGroupByVitamin;
